# container-camp-v4w-k3s-prep

This ansible installs the following:
* terraform
* k3s
* helm
* kubectl

## Auto-generating a hosts.yaml file from a vms4workshop csv (optional)

1. download the vmw4workshop csv file or paste the contents to `file.csv`
2. `ansible-playbook generate-hosts-from-csv.yaml`
3. the output will be a `hosts.yaml` that can be used into the prep playbook

Note to change the csv file and the output inventory file you can do the following:

`ansible-playbook -e csv-file=mycustomfile.csv -e output_inventory=myinventory.yaml generate-hosts-from-csv.yaml`

Just be sure to change the `-i` flag when running the prep playbook in the step below

## General steps to use this prep

1. `cp example-hosts.yaml hosts.yaml`
    1. add all the hosts; see hosts.yaml comments
    2. set the ansible_user
2. `export ANSIBLE_CONFIG=${PWD}/ansible.cfg`
3. `ansible-playbook -i hosts.yaml --forks=20 playbook.yaml`

optional, to reinstall k3s use the following commandline:

`ansible-playbook -i hosts.yaml -e k3s_reinstall=true --forks=20 playbook.yaml`